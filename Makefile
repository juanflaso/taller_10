#Escriba su makefiile en este archivo
#
# Luiggi Alarcón Gonzabay
# Juan Laso Delgado
FLAGS= gcc -Wall -c -I

all: bin/estatico bin/dinamico

bin/estatico: lib/libhashtab.a obj/prueba.o
	gcc -Wall obj/prueba.o -L lib/ -l hashtab -o $@ 

bin/dinamico: lib/libhashtab.so obj/prueba.o
	gcc -g -Wall obj/prueba.o -L lib/ -dynamic -l hashtab -o $@

lib/libhashtab.so: obj/hashDin.o obj/crearHashtableDin.o obj/numeroElementosDin.o obj/putDin.o obj/getDin.o obj/removerDin.o obj/borrarDin.o obj/clavesDin.o obj/contieneClaveDin.o obj/valoresDin.o
	gcc $^ -shared -o $@ 

lib/libhashtab.a: obj/hash.o obj/crearHashtable.o obj/numeroElementos.o obj/put.o obj/get.o obj/remover.o obj/borrar.o obj/claves.o obj/contieneClave.o obj/valores.o
	ar rcs $@ $^
	
obj/hash.o: src/hash.c
	$(FLAGS) include/ $< -o $@ -g
obj/crearHashtable.o: src/crearHashtable.c
	$(FLAGS) include/ $< -o $@ -g
obj/numeroElementos.o: src/numeroElementos.c
	$(FLAGS) include/ $< -o $@ -g
obj/put.o: src/put.c
	$(FLAGS) include/ $< -o $@ -g
obj/get.o: src/get.c
	$(FLAGS) include/ $< -o $@ -g
obj/remover.o: src/remover.c
	$(FLAGS) include/ $< -o $@ -g
obj/borrar.o: src/borrar.c
	$(FLAGS) include/ $< -o $@ -g
obj/claves.o: src/claves.c
	$(FLAGS) include/ $< -o $@ -g
obj/contieneClave.o: src/contieneClave.c
	$(FLAGS) include/ $< -o $@ -g
obj/valores.o: src/valores.c
	$(FLAGS) include/ $< -o $@ -g
obj/prueba.o: src/prueba.c
	$(FLAGS) include/ $< -o $@ -g

obj/hashDin.o: src/hash.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/crearHashtableDin.o: src/crearHashtable.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/numeroElementosDin.o: src/numeroElementos.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/putDin.o: src/put.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/getDin.o: src/get.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/removerDin.o: src/remover.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/borrarDin.o: src/borrar.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/clavesDin.o: src/claves.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/contieneClaveDin.o: src/contieneClave.c
	$(FLAGS) include/ -fPIC $< -o $@ -g
obj/valoresDin.o: src/valores.c
	$(FLAGS) include/ -fPIC $< -o $@ -g

.PHONY: clean 	
clean:
	rm obj/*
	rm bin/*

