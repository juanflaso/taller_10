#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "hashtable.h"

hashtable *crearHashTable(int numeroBuckets) {
    hashtable *table = malloc(sizeof(hashtable));

    if(table == NULL) {
        return NULL;
    }
    srand(time(NULL));
    table->id = rand();
    table->elementos = 0;
    table->numeroBuckets = numeroBuckets;
    table->buckets = malloc(numeroBuckets*sizeof(objeto*));

    for(int i = 0; i < numeroBuckets; i++){
        (table->buckets)[i] = NULL;
        
    }
	return table;
}


